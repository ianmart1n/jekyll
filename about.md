---
layout: page
title: About
permalink: /about/
---

FACILITATE is a co-operative dedicated to create gatherings of the diverse, in the spaces of the uncommon, focused by ART and CRITICAL THINKING

## Team

- Joseph Ianni
- Sandro Pehar
- Leelo Farhan


## Email

faciliatefacilitators@gmail.com


## Social media

- Facebook
- Twitter

## Website

Design by Ian Martin
